---
title: Gitlab info
weight: -20
---

## What is Gitlab

[![logo](/media/logo.png)](/media/logo.png)

### What is GitLab?

GitLab is a web-based Git repository that provides free open and private repositories, issue-following capabilities, and wikis. It is a complete DevOps platform that enables professionals to perform all the tasks in a project—from project planning and source code management to monitoring and security. Furthermore, it allows teams to collaborate and build better software.

### Why use GitLab?

The main benefit of using GitLab is that it allows all the team members to collaborate in every phase of the project. GitLab offers tracking from planning to creation to help developers automate the entire DevOps lifecycle and achieve the best possible results. More and more developers have started to use GitLab because of its wide features and brick blocks of code availability.

References


https://www.simplilearn.com/tutorials/git-tutorial/what-is-gitlab

https://en.wikipedia.org/wiki/GitLab
