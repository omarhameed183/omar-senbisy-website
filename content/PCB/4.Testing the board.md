---
title: 4. Testing the board
weight: -20
---
## Setting the Arduino

In order to upload a code on the ATtiny44 IC we have to set some settings first

### Arduino settings

Go to File>Preferences
and type in the **Additional Boards Manager URLS:** section the following link:
https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json


[![preferences](/media/preferences.png)](/media/preferences.png)

Now we have the board installed we have to choose it from Tools>Board>ATtiny microcontrollers> choose **ATtiny 24/44/84**

[![board](/media/board.png)](/media/board.png)

Then we choose the processor as **ATtiny44**

[![processor](/media/processor.png)](/media/processor.png)

We set the clock as **Internal 8 MHZ**

[![clock](/media/clock.png)](/media/clock.png)

Last thing we choose the Programmer as **Atmel-ICE (AVR)**

[![programmer](/media/programmer.png)](/media/programmer.png)

### Uploading demo code

The we open the **demo** code and make sure that the settings are the same.

### Conneting the board

We connect the Atmel-ICE to the six pins downward in the PCB as shown and according to the schematic

[![isp](/media/isp.png)](/media/isp.png)

[![connect](/media/connect.png)](/media/connect.png)

### Uploading the code

After that we just press upload and let the magic works
<!-- blank line -->
<figure class="video_container">
<iframe width="806" height="500" src="https://www.youtube.com/embed/HnS4C5c82hk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></figure>
<!-- blank line -->
