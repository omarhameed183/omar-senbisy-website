---
title: 2. PCB Fabrication
weight: -20
---
## Fabrication on Roland Modella MDX-20

[![MDX-20](/media/MDX-20.png)](/media/MDX-20.png)

### 1. Miling the traces
We mill the traces with an 0.4mm V shape bit and we open the http://fabmodules.org/ website in Ubuntu
then we change some parameters

[![traces](/media/traces.png)](/media/traces.png)

Then it will work perfectly

[![mill](/media/mill.png)](/media/mill.png)


### 2. Drilling

The following are the parameters for drilling

[![drilling](/media/drilling.png)](/media/drilling.png)

Then it will work perfectly

[![cut](/media/cut.png)](/media/cut.png)

The output is so neat as shown

[![prefinal](/media/prefinal.png)](/media/prefinal.png)

{{< toc >}}

